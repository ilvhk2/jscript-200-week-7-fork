/**
 * Car class
 * @constructor
 * @param {String} model
 */
class Car{
    constructor(model){
        this.speed = 0;
        this.model = model;
    }
    accelerate(){
        this.speed++;
    }
    brake(){
        this.speed--;
    }
    toString(){
        return(`${this.model} current speed is ${this.speed}`);
    }
}
//  Create an instance, accelerate twice, brake once, and console log the instance.toString()
    const myCar = new Car('Honda');
    myCar.accelerate();
    myCar.accelerate();
    myCar.brake();
    console.log(myCar.toString());
/**
 * ElectricCar class
 * @constructor
 * @param {String} model
 */
class ElectricCar extends Car {
    constructor(model){
        super(model);
        this.motor = 'electric';
    }
    accelerate(){
        super.accelerate();
        super.accelerate();
    }
    toString(){
         return(`Thie electric ${this.model} current speed is ${this.speed}`);
    }
}
//  Create an instance, accelerate twice, brake once, and console log the instance.toString()
const myEcar = new ElectricCar('Toyota');
myEcar.accelerate();
myEcar.accelerate();
myEcar.brake();
console.log(myEcar.toString());