const form = document.querySelector('form');
const emailReg = /\w+@\w+\.\w+/;

form.addEventListener('submit', function(e){
    const chkInput = document.getElementsByClassName('validate-input');
    Array.from(chkInput).forEach( i => {
         // console.log('type =' + i.type);
        if (i.type == 'text' && i.value.length <3) { 
                e.preventDefault();
                i.parentElement.classList.add('invalid');
                i.nextElementSibling.innerText= ` *** Invalid ${i.name} ***`;
        } else if (i.type =='email') {
                if ( i.value.length >0 && emailReg.test(i.value)){
                    i.parentElement.classList.remove('invalid');
                    i.nextElementSibling.innerText='';
                } else {
                    e.preventDefault();
                    i.parentElement.classList.add('invalid');
                    i.nextElementSibling.innerText= ` *** Invalid ${i.name} ***`;
                    console.dir( i.parentElement );
                }
        } else{
            i.parentElement.classList.remove('invalid');
            i.nextElementSibling.innerText='';
        }
    });
})



