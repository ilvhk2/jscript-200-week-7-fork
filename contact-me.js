const form = document.querySelector('form');
const emailReg = /\w+@\w+\.\w+/;

const selectEl = document.getElementById('contact-reason');
const jobEl = document.getElementById('job');
const talkEl = document.getElementById('talk');

const setSelectValidity = function(){
    if(selectEl.value === 'choose'){
        selectEl.setCustomValidity("Must select an option");
        return;
    }
    if(selectEl.value === 'job'){
        jobEl.classList.remove('hide');
        talkEl.classList.add('hide');
    }else{
        jobEl.classList.add('hide');
        talkEl.classList.remove('hide');
    }
};
selectEl.addEventListener('change',setSelectValidity);


form.addEventListener('submit', function(e){
    const error = [];
    const chkInput = document.getElementsByClassName('validate-input');
    Array.from(chkInput).forEach( i => {
         // console.log('type =' + i.type);
        if (i.type == 'text' && i.value.length <3) { 
                e.preventDefault();
                i.parentElement.classList.add('invalid');
                i.nextElementSibling.innerText= ` *** Invalid ${i.name} ***`;
                error.push(`Invalid ${i.name}`);
        }        else{
            i.parentElement.classList.remove('invalid');
            i.nextElementSibling.innerText='';
        }

        if (i.type =='email') {
                if ( i.value.length >0 && emailReg.test(i.value)){
                    i.parentElement.classList.remove('invalid');
                    i.nextElementSibling.innerText='';
                } 
                else {
                    e.preventDefault();
                    i.parentElement.classList.add('invalid');
                    i.nextElementSibling.innerText= ` *** Invalid ${i.name} ***`;
                    //console.dir( i.parentElement );
                    error.push(`Invalid ${i.name}`);
                }
        }
        if (i.type == "textarea"  &&  i.textLength < 10) {   
            e.preventDefault(); 
            error.push('Message contains less than 10 characters');
            i.classList.add('invalid');
        }   else{
            i.classList.remove('invalid');
        }

    });

    if( error.length > 0) {
        alert(error.join().split(",").join("\n"));
     }
})





