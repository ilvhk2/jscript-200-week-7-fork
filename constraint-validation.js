const selectEl = document.getElementById('contact-kind');

const setSelectValidity = function(){
    if(selectEl.value === 'choose'){
        selectEl.setCustomValidity("Must select an option");
    }else{
        selectEl.setCustomValidity('');
    }
};

/*----------------   ------------------*/
setSelectValidity();
selectEl.addEventListener('change',setSelectValidity);

const form = document.getElementById('connect-form');

/*-------------------- checkValidity() --------------
form.addEventListener('submit', function(){
    const inputs = document.getElementsByClassName('validate-input');
    Array.from(inputs).forEach( i => {
           console.log(i.checkValidity());    // return true or false
    });
    e.preventDefault();                  
})
*/
/*----------------------------
form.addEventListener('submit', function(){
    const inputs = document.getElementsByClassName('validate-input');     //  class on elements: input & select
    let formIsValid = true;             ///------
    Array.from(inputs).forEach( i => {                 
        if ( !i.checkValidity()  ) {
            console.dir(i.classList);
            i.classList.add('invalid');  
            formIsValid = false;
    });
    if (!formIsValid ) {            //form is not valid, then prevent it from submitting
        e.preventDefault();
    }
})
-*/

/*------------------------ validationMessage ------------*/
form.addEventListener('submit', function(){
    const inputs = document.getElementsByClassName('validate-input');
    let formIsValid = true;

    Array.from(inputs).forEach( i => {
        const small = i.parentElement.querySelector('small');
           //console.log(i.checkValidity());
        if ( !i.checkValidity()  ) {
            formIsValid = false;
            console.log(i.validationMessage);  //-----------------
           // i.parentElement.querySelector('small').innerText = i.validationMessage;
           small.innerText = i.validationMessage;
        } else {
            small.innerText = '';
         }
    });
    if (!formIsValid ) {
        e.preventDefault();
    }
});

/*------------------------ validationMessage ------------  
// <small class="error"> This is custom error message</small>

form.addEventListener('submit', function(){
    const inputs = document.getElementsByClassName('validate-input');
    let formIsValid = true;

    Array.from(inputs).forEach( i => {
        const small = i.parentElement.querySelector('small');
           //console.log(i.checkValidity());
        if ( !i.checkValidity()  ) {
            formIsValid = false;
           // console.log(i.validationMessage);  //-----------------
            i.parentElement.querySelector('small').innerText = i.validationMessage;
           small.innerText = i.validationMessage;
           i.classList.add('invalid');        // JS: add a class -- use classList
        } else {
            small.innerText = '';
            i.classList.remove('invalid');
         }
    }) 
    if (!formIsValid ) {
        e.preventDefault();
    }
})
*/

/*-----------------------
const form = document.querySelector('form');
form.addEventListener('submit', function(e){
    const fn = document.getElementById('first-name');
    if  ( fn.value.length <5 ){
        e.preventDefault();
        fn.nextElementSibling.innerHTML='invalid entry';
        fn.nextElementSibling.classList.add('invalid');
    }
})
*/ 
